package com.company;
import java.util.ArrayList;

public class ParkingPlaces {
    private int capacity;
    private int whenAvailable;
    private String parkingType;
    ArrayList<Car> places;

    public ParkingPlaces(int capacity, String parkingType) {
        this.capacity = capacity;
        this.whenAvailable = 0;
        this.parkingType = parkingType;
        this.places = new ArrayList<Car>(capacity);
    }

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getParkingType() {
        return parkingType;
    }

    public void setParkingType(String parkingType) {
        this.parkingType = parkingType;
    }

    public int getWhenAvailable() {
        return whenAvailable;
    }

    public void setWhenAvailable(int whenAvailable) {
        this.whenAvailable = whenAvailable;
    }


    public int isAvailablePlaceForTruck() {
        for (int j = 0; j < this.capacity - 1; j++) {
            if(this.places.get(j) == null){
                if(this.places.get(j) == null){
                    return j;
                }
            }
        }
        return -1;
    }

    public int returnAvailablePlace(){
        for(int j = 0; j < this.capacity; j++){
            if(this.places.get(j) == null){
                return j;
            }
        }
        return -1;
    }

    public void park(Car car, int steps, int k){
        this.places.set(k, car);
        this.places.get(k).setSteps(steps);
        this.places.get(k).setPlace(k+1);
        if(this.places.size() == this.capacity ){
            if(steps <= this.whenAvailable){
                setWhenAvailable(steps);
            }
        }
    }

    public void parkTruckOnCar(Car truck, int steps, int k){
        this.places.set(k, truck);
        this.places.get(k).setSteps(steps);
        this.places.get(k).setPlace(k+1);
    }

    public void endRound(){
        for(int j = 0; j < this.capacity; j++) {
            if(this.places.get(j) != null){
                this.places.get(j).setSteps(this.places.get(j).getSteps()-1);
                if(getParkingType().equals("car")&(this.places.get(j).getType().equals("truck"))){
                    j++;
                }
                setWhenAvailable(0);
            }
        }
    }


    public void findVehicle(int id){
        int isThere = 0;
        for(int j = 0; j < this.capacity; j++){
            if(this.places.get(j) != null){
                if(this.places.get(j).getId() == id){
                    System.out.println(this.places.get(j).getType() + " is in the " + parkingType + "'s parking on place #" + (j+1) + " for " + this.places.get(j).getSteps() + " steps");
                    isThere++;
                }
            }
        }
        if(isThere == 0){
            System.out.println("Didn't find it in the " + parkingType + "'s parking");
        }
    }

    public void clearParking(){
        for(int j = 0; j < this.capacity; j++){
            if(this.places.get(j) != null) {
                this.places.get(j).setSteps(0);
                this.places.get(j).setPlace(0);
                this.places.set(j, null);
            }
        }
    }

    public void camera(){
        for(int j = 0; j < this.capacity; j++) {
            if (this.parkingType.equals("car")){
                    System.out.println(" ---------------------------------- ");
                if (this.places.get(j) != null) {
                    if (this.places.get(j).getType().equals("car")) {
                        System.out.println("|" + ANSI_PURPLE + "  ====        ====" + ANSI_RESET);
                        System.out.println("|" + ANSI_CYAN + " -------------------" + ANSI_RESET);
                        System.out.println("|" + ANSI_CYAN + "|                   |" + ANSI_RESET);
                        System.out.println("|" + ANSI_CYAN + "|     " + ANSI_BLUE + "Car #" + this.places.get(j).getId() + ANSI_CYAN + "     |" + ANSI_BLUE + "   [" + this.places.get(j).getSteps() + "]" + ANSI_RESET);
                        System.out.println("|" + ANSI_CYAN + "|                   |" + ANSI_RESET);
                        System.out.println("|" + ANSI_CYAN + " -------------------" + ANSI_RESET);
                        System.out.println("|" + ANSI_PURPLE + "  ====        ====" + ANSI_RESET);
                    } else if (this.places.get(j).getType().equals("truck")) {
                        System.out.println("|");
                        System.out.println("|");
                        System.out.println("|" + ANSI_PURPLE + "  =====          =====" + ANSI_RESET);
                        System.out.println("|" + ANSI_CYAN + " -----------------------" + ANSI_RESET);
                        System.out.println("|" + ANSI_CYAN + "|                       |" + ANSI_RESET);
                        System.out.println("|" + ANSI_CYAN + "|                       |" + ANSI_RESET);
                        System.out.println("|" + ANSI_CYAN + "|                       |" + ANSI_RESET);
                        System.out.println("|" + ANSI_CYAN + "|      " + ANSI_BLUE + "Truck #" + this.places.get(j).getId() + ANSI_CYAN + "      |" + ANSI_RESET + "---" + ANSI_BLUE + "[" + this.places.get(j).getSteps() + "]" + ANSI_RESET + "---");
                        System.out.println("|" + ANSI_CYAN + "|                       |" + ANSI_RESET);
                        System.out.println("|" + ANSI_CYAN + "|                       |" + ANSI_RESET);
                        System.out.println("|" + ANSI_CYAN + "|                       |" + ANSI_RESET);
                        System.out.println("|" + ANSI_CYAN + " -----------------------" + ANSI_RESET);
                        System.out.println("|" + ANSI_PURPLE + "  =====          =====" + ANSI_RESET);
                        System.out.println("|");
                        System.out.println("|");
                        j++;
                    }
                }
                else {
                    System.out.println("|");
                    System.out.println("|");
                    System.out.println("|");
                    System.out.println("|");
                    System.out.println("|");
                    System.out.println("|");
                    System.out.println("|");
                }
                System.out.println(" ---------------------------------- ");
            }
            if (this.parkingType.equals("truck")){
                System.out.println(" ---------------------------------- ");
                if (this.places.get(j) != null) {
                    System.out.println("|" + ANSI_PURPLE + "  =====          =====" + ANSI_RESET);
                    System.out.println("|" + ANSI_CYAN + " -----------------------" + ANSI_RESET);
                    System.out.println("|" + ANSI_CYAN + "|                       |" + ANSI_RESET);
                    System.out.println("|" + ANSI_CYAN + "|                       |" + ANSI_RESET);
                    System.out.println("|" + ANSI_CYAN + "|                       |" + ANSI_RESET);
                    System.out.println("|" + ANSI_CYAN + "|      " + ANSI_BLUE + "Truck #" + this.places.get(j).getId() + ANSI_CYAN + "      |" + ANSI_BLUE + "   [" + this.places.get(j).getSteps() + "]" + ANSI_RESET);
                    System.out.println("|" + ANSI_CYAN + "|                       |" + ANSI_RESET);
                    System.out.println("|" + ANSI_CYAN + "|                       |" + ANSI_RESET);
                    System.out.println("|" + ANSI_CYAN + "|                       |" + ANSI_RESET);
                    System.out.println("|" + ANSI_CYAN + " -----------------------" + ANSI_RESET);
                    System.out.println("|" + ANSI_PURPLE + "  =====          =====" + ANSI_RESET);
                }
                else {
                    System.out.println("|");
                    System.out.println("|");
                    System.out.println("|");
                    System.out.println("|");
                    System.out.println("|");
                    System.out.println("|");
                    System.out.println("|");
                    System.out.println("|");
                    System.out.println("|");
                    System.out.println("|");
                    System.out.println("|");
                }
                System.out.println(" ---------------------------------- ");
            }
        }
    }

    public void getInfo(int k){
        if((k>this.capacity)|(k<1)){
            System.out.println("There is no place #" + k + " in " + getParkingType() + "-parking");
        }
        else if(this.places.get(k-1) == null){
            System.out.println("The place #" + k + " of " + getParkingType() + "-parking is empty");
        }
        else{
            System.out.println("Place #" + k + " of " + getParkingType() + "-parking is occupied by " + this.places.get(k-1).getType() + " identified as " + this.places.get(k-1).getId() + " for next " + this.places.get(k-1).getSteps() + " steps");
        }
    }

    public void getInfoAll(){
        System.out.println(getParkingType() + "-parking:");
        System.out.println("----------------------------");
        for(int j = 0; j < this.capacity; j++) {
            if (this.places.get(j-1) == null) {
                System.out.println("#" + (j+1) + " - empty");
            } else {
                System.out.println("#" + (j+1) + " - " + this.places.get(j-1).getType() + " (id " + this.places.get(j-1).getId() + ") for " + this.places.get(j-1).getSteps() + " steps");
            }
        }
        System.out.println("");
    }
}

