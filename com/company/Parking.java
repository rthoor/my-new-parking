package com.company;

import java.util.Random;
import java.util.Scanner;


public class Parking {
    private String name;
    private boolean isOn;
    public Parking(String name) {
        this.name = name;
        this.isOn = true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOn() {
        return isOn;
    }

    public void setOn(boolean on) {
        isOn = on;
    }

    public void help(){
        System.out.println("Available commands:");
        System.out.println("------------------");
        System.out.println(ANSI_YELLOW + "continue");
        System.out.println("stop");
        System.out.println("status");
        System.out.println("camera");
        System.out.println("find");
        System.out.println("info");
        System.out.println("clear" + ANSI_RESET);
        System.out.println("");
    }

    public void options(){
        System.out.println("Available commands:");
        System.out.println("------------------");
        System.out.println(ANSI_YELLOW + "car");
        System.out.println("truck" + ANSI_RESET);
        System.out.println("");
    }

    public void optionsInfo(){
        System.out.println("Available commands:");
        System.out.println("------------------");
        System.out.println(ANSI_YELLOW + "car");
        System.out.println("truck");
        System.out.println("all" + ANSI_RESET);
        System.out.println("");
    }

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Parking parking = new Parking("my parking");
        System.out.println("Salam, enter car-parking's capacity");
        int carCapacity = scanner.nextInt();
        ParkingPlaces carPark = new ParkingPlaces(carCapacity, "car");
        carPark.fillParking();
        System.out.println("Enter truck-parking's capacity");
        int truckCapacity = scanner.nextInt();
        ParkingPlaces truckPark = new ParkingPlaces(truckCapacity, "truck");
        truckPark.fillParking();
        Random random = new Random();
        int id = 1000;
        while(parking.isOn()){
            carPark.updateStatus();
            truckPark.updateStatus();
            parking.help();
            System.out.println("Enter command");
            String command = scanner.nextLine();
            switch(command){
                case("stop"):{
                    parking.setOn(false);
                    break;
                }
                case("continue"):{
                    int carAmount = random.nextInt((carCapacity+truckCapacity)/3+1);
                    while(carAmount > 0){
                        Car car = new Car(id, "car");
                        if(carPark.places.contains(null)) {
                            int steps = random.nextInt(10) + 1;
                            int k = carPark.returnAvailablePlace();
                            carPark.park(car, steps, k);
                            System.out.println(ANSI_GREEN + "You successfully parked car identified as " + id + " on place #" + car.getPlace() + " for " + car.getSteps() + " steps" + ANSI_RESET);
                        }
                        else{
                            System.out.println(ANSI_RED + "Sorry, there aren't any available parking places for cars");
                            System.out.println("Please, wait " + carPark.getWhenAvailable() + " steps" + ANSI_RESET);
                            break;
                        }
                        carAmount--;
                        carPark.updateStatus();
                        truckPark.updateStatus();
                        id++;
                    }
                    int truckAmount = random.nextInt((carCapacity+truckCapacity)/3+1);
                    while(truckAmount > 0){
                        Car truck = new Car(id, "truck");
                        if(truckPark.places.contains(null)) {
                            int steps = random.nextInt(10) + 1;
                            int k = truckPark.returnAvailablePlace();
                            truckPark.park(truck, steps, k);
                            System.out.println(ANSI_GREEN + "You successfully parked truck identified as " + id + " on place #" + truck.getPlace() + " for " + truck.getSteps() + " steps" + ANSI_RESET);
                        }
                        else if(carPark.isAvailablePlaceForTruck() >= 0){
                            int steps = random.nextInt(10) + 1;
                            int place = carPark.isAvailablePlaceForTruck();
                            carPark.parkTruckOnCar(truck, steps, place);
                            carPark.parkTruckOnCar(truck, steps, place+1);
                            System.out.println(ANSI_GREEN + "You successfully parked truck identified as " + id + " on place #" + truck.getPlace() + " and #" + (truck.getPlace()+1) + " for " + truck.getSteps() + " steps on car parking" + ANSI_RESET);
                        }
                        else{
                            System.out.println(ANSI_RED + "Sorry, there aren't any available parking places for trucks");
                            System.out.println("Please, wait " + truckPark.getWhenAvailable() + " steps" + ANSI_RESET);
                            break;
                        }
                        truckAmount--;
                        truckPark.updateStatus();
                        id++;
                    }
                    carPark.endRound();
                    truckPark.endRound();
                    break;
                }
                case("status"):{
                    System.out.println("Choose parking");
                    parking.options();
                    String type = scanner.nextLine();
                    switch(type){
                        case("car"):{
                            carPark.status();
                            break;
                        }
                        case("truck"):{
                            truckPark.status();
                            break;
                        }
                    }
                    break;
                }
                case("camera"):{
                    System.out.println("Choose parking");
                    parking.options();
                    String type = scanner.nextLine();
                    switch(type) {
                        case ("car"): {
                            carPark.camera();
                            break;
                        }
                        case ("truck"): {
                            truckPark.camera();
                            break;
                        }
                    }
                    break;
                }
                case("find"):{
                    System.out.println("Enter vehicle's ID");
                    int vehicleID = scanner.nextInt();
                    carPark.findVehicle(vehicleID);
                    truckPark.findVehicle(vehicleID);
                    break;
                }
                case("info"):{
                    System.out.println("Choose parking");
                    parking.optionsInfo();
                    String type = scanner.nextLine();
                    switch(type) {
                        case ("car"): {
                            System.out.println("Enter place's #");
                            int place = scanner.nextInt();
                            carPark.getInfo(place);
                            break;
                        }
                        case ("truck"): {
                            System.out.println("Enter place's #");
                            int place = scanner.nextInt();
                            truckPark.getInfo(place);
                            break;
                        }
                        case ("all"): {
                            carPark.getInfoAll();
                            truckPark.getInfoAll();
                            break;
                        }
                    }
                    break;
                }
                case("clear"):{
                    System.out.println("Choose parking");
                    parking.options();
                    String type = scanner.nextLine();
                    switch(type) {
                        case ("car"): {
                            carPark.clearParking();
                            System.out.println("Car parking is empty now!");
                            break;
                        }
                        case ("truck"): {
                            truckPark.clearParking();
                            System.out.println("Truck parking is empty now!");
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }
}
