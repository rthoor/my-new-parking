package com.company;

public class Car {
    private int id;
    private int steps;
    private String type;
    private int place;

    public Car(int id, String type) {
        this.id = id;
        this.steps = 0;
        this.type = type;
        this.place = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSteps() {
        return steps;
    }

    public void setSteps(int steps) {
        this.steps = steps;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }
}

